package validator

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestValidUrl(t *testing.T) {
	urls := []string{"dev.commonground.nl", "mijn-organisatie.nl", "http://www.dev.commonground.nl", "l.nl"}

	for _, url := range urls {
		valid := ValidUrl(url)
		assert.True(t, valid)
	}
}

func TestInvalidUrl(t *testing.T) {
	urls := []string{"devcommongroundnl", "$#^&.nl", ".nl"}

	for _, url := range urls {
		valid := ValidUrl(url)
		assert.False(t, valid)
	}
}
