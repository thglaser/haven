package addons

import (
	"encoding/json"
	"fmt"
	"github.com/AlecAivazis/survey/v2"
	"github.com/google/uuid"
	"github.com/gookit/color"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/certificates"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/handler"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/helm"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes"
	certv1 "gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes/models/crds/cert/v1"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/validator"
	"io"
	"io/ioutil"
	"net/url"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

type NLXAddon struct {
	Addon           Addon
	FilteredAddons  FilteredAddons
	Kube            kubernetes.KubeClient
	KubeImpl        kubernetes.Kube
	CustomValues    map[string]interface{}
	Inway           bool
	Outway          bool
	Postgres        bool
	OrgName         string
	CommonNameCerts string
	CertDir         string
	SerialNumber    string
	Certs           NLXCerts
}

type FilteredAddons struct {
	Inway       NestedChart
	Outway      NestedChart
	Management  NestedChart
	Postgres    NestedChart
	CertManager NestedChart
}

type NLXCerts struct {
	CaCert  []byte
	CaKey   []byte
	OrgCert []byte
	OrgKey  []byte
	OrgCsr  []byte
}

type ExternalCert struct {
	Certificate string `json:"certificate"`
}

const (
	CERTIFICATES     = "cert-manager"
	POSTGRES         = "postgresql"
	NLXMANAGEMENT    = "nlx-management"
	INWAY            = "INWAY"
	OUTWAY           = "OUTWAY"
	INSTALLPOSTGRES  = "INSTALLPOSTGRES"
	CERTDIR          = "CERTDIR"
	ORGNAME          = "ORGNAME"
	LETSENCRYPTNAME  = "letsencrypt-prod"
	INTERNALNAME     = "internalissuer"
	CAKEYNAME        = "ca.key"
	CACRTNAME        = "ca.crt"
	ORGCSRMAME       = "org.csr"
	ORGKEYNAME       = "org.key"
	ORGCERTNAME      = "org.crt"
	ROOTCERTNAME     = "root.crt"
	SCHEME           = "https"
	HOST             = "certportal.demo.nlx.io"
	REQUESTPATH      = "api/request_certificate"
	defaultNamespace = "nlx"
)

func (n NLXAddon) InstallNLXHelmCharts() {
	logging.Info("Creating nlx addon")

	logging.Warning(color.Red.Sprintf("Make sure you have a (sub) domain name ready with the ability to alter DNS records that can be used to make the NLX Inway publicly available."))
	logging.Info("For more information please refer to %s", "https://docs.nlx.io/try-nlx/helm/preparation")

	err := n.CreateNLXInstallConfig()
	if err != nil {
		return
	}

	n.FilterAddons()

	logging.Info("Working on step 1: creating root namespace")
	err = n.Kube.Namespaces().Create(n.Addon.Namespace)
	if err != nil {
		logging.Error("Error occurred getting or creating namespace %s\n", err)
		return
	}

	err = n.CreateCertManager()
	if err != nil {
		return
	}

	err = n.CreateCertifications()
	if err != nil {
		return
	}

	if n.Postgres {
		err = n.AddPostgres()
		if err != nil {
			return
		}
	}

	err = n.ApplyNlxManagement()
	if err != nil {
		return
	}

	if n.Inway {
		err = n.ApplyInway()
		if err != nil {
			return
		}
	}

	if n.Outway {
		err = n.ApplyOutway()
		if err != nil {
			return
		}
	}
}

func RemoveNLXInstall() {
	// remove helm charts that are part of NLX
	// postgres - management - inway - outway
	uninstallValues := map[string]interface{}{}

	var transform func(s interface{}) interface{}
	logging.Info("What would you like to uninstall?")
	surveyQuestions := []*survey.Question{
		{
			Name: "REMOVENAMESPACE",
			Prompt: &survey.Input{
				Message: "Also remove the NLX namespace?",
				Help:    "Only remove the NLX namespace if that was created during the install of a test addon",
				Default: "yes",
			},
			Transform: transform,
		},
		{
			Name: "POSTGRESQL",
			Prompt: &survey.Input{
				Message: "Remove postgresql from given namespace?",
				Help:    "If you installed a test cluster it is safe to remove the postgres instance",
				Default: "yes",
			},
			Transform: transform,
		},
	}
	err := survey.Ask(surveyQuestions, &uninstallValues)
	if err != nil {
		logging.Error("Error processing answers to questionnaire: %s\n", err)
		return
	}

	namespace := defaultNamespace
	removePostgres := determineTextBaseFlags("POSTGRESQL", uninstallValues["POSTGRESQL"])
	removeNamespace := determineTextBaseFlags("REMOVENAMESPACE", uninstallValues["REMOVENAMESPACE"])

	kubePath, _ := kubernetes.ReadK8sConfigFromPath()
	kubeImpl, err := kubernetes.New(kubePath)
	if err != nil {
		logging.Error("Error creating kubeImpl: %s\n", err)
		return
	}
	kube, err := kubernetes.NewClientFromFile(kubePath)
	if err != nil {
		logging.Error("Error creating kubeClient: %s\n", err)
		return
	}

	logging.Info("Using the following namespace: %s", namespace)
	helmClient, err := helm.NewClient(namespace, kubeImpl)
	if err != nil {
		logging.Error("Error creating helm client %s", err)
		return
	}

	releases, err := helmClient.List()
	if err != nil {
		logging.Error("Error helm listing %s", err)
		return
	}

	for _, release := range releases {
		if release.Name == POSTGRES {
			if !removePostgres {
				logging.Info("Leaving chart %s in namespace %s", POSTGRES, namespace)
				continue
			}
		}
		logging.Info("Removing helm chart: %s", release.Name)
		_, err := helmClient.Uninstall(release.Name)
		if err != nil {
			logging.Error("Error removing helm chart: %s\n", err)
			return
		}
		logging.Info("Removed helm chart: %s", release.Name)
	}

	//Certificate
	crds, err := kube.CustomResourceDefinitions().List()
	if err != nil {
		logging.Error("Error getting crds: %s\n", err)
		return
	}

	resources := []string{
		"Certificate",
		"Issuer",
	}

	for _, crd := range crds.Items {
		for _, resource := range resources {
			if crd.Spec.Names.Kind == resource {
				version := crd.Spec.Versions[len(crd.Spec.Versions)-1].Name
				subs, _ := kube.CustomResources().List(crd.Spec.Group, version, crd.Spec.Names.Plural)

				for _, sub := range subs.Items {
					jsonObject, _ := json.Marshal(sub)
					certResource, _ := certv1.UnmarshalCertificateResource(jsonObject)
					if certResource.Object.Metadata.Namespace == namespace {
						crdName := certResource.Object.Metadata.Name

						logging.Info("Deleting crd resource %s named %s in namespace %s", resource, crdName, namespace)
						err := kube.CustomResources().Delete(crd.Spec.Group, version, crd.Spec.Names.Plural, certResource.Object.Metadata.Namespace, crdName)
						if err != nil {
							logging.Error("Error deleting crd resource %s: %s\n", crdName, err)
							return
						}
					}
				}
			}
		}
	}

	//jobs
	jobs, err := kube.Jobs().List(namespace)
	if err != nil {
		logging.Error("Error getting jobs: %s\n", err)
		return
	}

	for _, job := range jobs.Items {
		logging.Info("Deleting job: %s in namespace: %s", job.Name, job.Namespace)
		err := kube.Jobs().Delete(namespace, job.Name, nil)
		if err != nil {
			logging.Error("Error deleting job %s: %s\n", job.Name, err)
			return
		}
	}

	//secrets
	secrets, err := kube.Secrets().List(namespace)
	if err != nil {
		logging.Error("Error getting secrets: %s\n", err)
		return
	}

	for _, secret := range secrets.Items {
		if secret.Type == "kubernetes.io/service-account-token" {
			continue
		}

		if secret.Name == POSTGRES && !removePostgres {
			continue
		}

		logging.Info("Deleting secret: %s in namespace: %s", secret.Name, secret.Namespace)
		err = kube.Secrets().Delete(namespace, secret.Name)
		if err != nil {
			logging.Error("Error deleting secret %s: %s\n", secret.Name, err)
			return
		}
	}

	// create-user pod
	pods, err := kube.Pods().List(namespace)
	if err != nil {
		logging.Error("Error getting pods: %s\n", err)
		return
	}

	for _, pod := range pods.Items {
		if strings.Contains(pod.Name, "api-create-user") {
			logging.Info("Deleting pod: %s in namespace: %s", pod.Name, pod.Namespace)
			err = kube.Pods().Delete(namespace, pod.Name)
			if err != nil {
				logging.Error("Error deleting pod %s: %s\n", pod.Name, err)
				return
			}
		}
	}

	//pv and pvc
	if removePostgres {
		pvcs, err := kube.VolumeClaims().List(namespace)
		if err != nil {
			logging.Error("Error getting pods: %s\n", err)
			return
		}

		for _, claim := range pvcs.Items {
			if strings.Contains(claim.Name, "data-postgresql") {
				logging.Info("Deleting pvc: %s in namespace: %s", claim.Name, claim.Namespace)
				logging.Info("This will also delete the underlying PersistentVolume")
				err = kube.VolumeClaims().Delete(claim.Namespace, claim.Name)
				if err != nil {
					logging.Error("Error deleting pvc %s: %s\n", claim.Name, err)
					return
				}
			}
		}
	}

	//namespace
	if removeNamespace {
		logging.Info("Removing namespace: %s", namespace)
		err := kube.Namespaces().Delete(namespace)
		if err != nil {
			logging.Error("Error deleting namespace %s: %s\n", namespace, err)
			return
		}

	} else {
		logging.Info("Leaving namespace: %s. If you want to delete this run:\n"+
			"$ kubectl delete ns %s", namespace, namespace)
	}
}

func UpgradeNLXInstall() {
	// upgrade helm charts that are part of NLX
	// management - inway - outway
	namespace := defaultNamespace

	kubePath, _ := kubernetes.ReadK8sConfigFromPath()
	kubeImpl, err := kubernetes.New(kubePath)
	if err != nil {
		logging.Error("Error creating kube client %s", err)
		return
	}

	logging.Info("Using the following namespace: %s", namespace)
	helmClient, err := helm.NewClient(namespace, kubeImpl)
	if err != nil {
		logging.Error("Error creating helm client %s", err)
		return
	}

	releases, err := helmClient.List()
	if err != nil {
		logging.Error("Error helm listing %s", err)
		return
	}

	nlxRepos := []string{
		"inway",
		"outway",
		"management",
	}

	for _, release := range releases {
		for _, repo := range nlxRepos {
			if strings.Contains(release.Name, repo) {
				logging.Info("Upgrading helm chart: %s", release.Name)
				logging.Info("Current app version %s", release.Chart.AppVersion())
				logging.Info("Last upgrade or install done at: %s", release.Info.LastDeployed.String())
				chartName := fmt.Sprintf("%s/%s", "commonground", release.Name)
				r, err := helmClient.Upgrade(release.Name, chartName, nil)
				if err != nil {
					logging.Error("Error upgrading helm chart %s: %s\n", chartName, err)
					return
				}
				logging.Info("Upgraded helm chart: %s revision upgraded from %d to %d", release.Name, release.Version, r.Version)
				logging.Info("Current app version %s", r.Chart.AppVersion())
			}
		}
	}
}

func (n *NLXAddon) CreateNLXInstallConfig() error {
	customValues := map[string]interface{}{}
	postgresValues := map[string]interface{}{}
	err := survey.Ask(n.Addon.GetSurveyQuestions(), &customValues)
	if err != nil {
		logging.Error("Error processing answers: %s\n", err)
		return err
	}

	for key, value := range customValues {
		if key == INWAY {
			n.Inway = determineTextBaseFlags(key, value)
		}
		if key == OUTWAY {
			n.Outway = determineTextBaseFlags(key, value)
		}
		if key == INSTALLPOSTGRES {
			n.Postgres = determineTextBaseFlags(key, value)
			if !n.Postgres {
				var transform func(s interface{}) interface{}
				logging.Info("You are using an existing postgres more info is needed.")
				surveyQuestions := []*survey.Question{
					{
						Name: "%POSTGRESHOST%",
						Prompt: &survey.Input{
							Message: "hostname of existing postgresql",
							Default: "postgresql",
						},
						Transform: transform,
					},
					{
						Name: "%POSTGRESDATABASE%",
						Prompt: &survey.Input{
							Message: "Database of existing postgresql",
							Default: "nlx_management",
						},
						Transform: transform,
					},
					{
						Name: "%POSTGRESUSER%",
						Prompt: &survey.Input{
							Message: "User of existing postgresql",
							Default: "postgres",
						},
						Transform: transform,
					},
					{
						Name: "%POSTGRESPASSWORD%",
						Prompt: &survey.Input{
							Message: "Password of existing postgresql",
							Default: "supersecretpassword",
						},
						Transform: transform,
					},
				}
				err = survey.Ask(surveyQuestions, &postgresValues)
				if err != nil {
					logging.Error("Error processing answers to questionnaire: %s\n", err)
					return err
				}
			} else {
				postgresValues["%POSTGRESHOST%"] = "postgresql"
				postgresValues["%POSTGRESUSER%"] = "postgres"
				postgresValues["%POSTGRESDATABASE%"] = "nlx_management"
			}
		}
		if key == CERTDIR {
			n.CertDir = fmt.Sprintf("%v", value)
		}
		if key == ORGNAME {
			n.OrgName = fmt.Sprintf("%v", value)
		}
	}

	logging.Info("Creating %s = %s", INWAY, strconv.FormatBool(n.Inway))
	logging.Info("Creating %s = %s", OUTWAY, strconv.FormatBool(n.Outway))

	validInwayUrl := validator.ValidUrl(fmt.Sprintf("%v", customValues["%INWAYURL%"]))
	if !validInwayUrl {
		logging.Error(color.Red.Sprintf("INWAYURL is not valid! Aborting install. Valid example: mijn-organisatie.nl"))
	}

	validManagementUrl := validator.ValidUrl(fmt.Sprintf("%v", customValues["%MANAGEMENTURL%"]))
	if !validManagementUrl {
		logging.Error(color.Red.Sprintf("MANAGEMENTURL is not valid! Aborting install. Valid example: mijn-organisatie.nl"))
	}

	orgNameValid := validator.ValidOrganization(n.OrgName)
	if !orgNameValid {
		logging.Error(color.Red.Sprintf("ORGNAME is not valid! Aborting install. Valid example: mijn-organisatie"))
	}

	n.CommonNameCerts = fmt.Sprintf("%v", customValues["%INWAYURL%"])

	cv := map[string]interface{}{}
	cv["%CLUSTERISSUER_EMAIL%"] = customValues["%CLUSTERISSUER_EMAIL%"]
	cv["%SELFADDRESS%"] = fmt.Sprintf("%s:443", customValues["%INWAYURL%"])
	cv["%INWAYURL%"] = customValues["%INWAYURL%"]
	cv["%MANAGEMENTURL%"] = customValues["%MANAGEMENTURL%"]
	cv["%CLUSTER_FRIENDLY_ORG_NAME%"] = n.OrgName
	cv["%ADMINPASSWORD%"] = customValues["%ADMINPASSWORD%"]
	cv["%ADMINUSER%"] = customValues["%ADMINUSER%"]
	cv["%POSTGRESHOST%"] = postgresValues["%POSTGRESHOST%"]
	cv["%POSTGRESUSER%"] = postgresValues["%POSTGRESUSER%"]
	cv["%POSTGRESDATABASE%"] = postgresValues["%POSTGRESDATABASE%"]
	if postgresValues["%POSTGRESPASSWORD%"] != nil {
		cv["%POSTGRESPASSWORD%"] = postgresValues["%POSTGRESPASSWORD%"]
	}
	n.CustomValues = cv

	if n.CertDir == "/tmp" {
		logging.Info("Creating directory in /tmp dir to store certificates generated during this addon install")
		id := uuid.New()
		filePath := filepath.Join(n.CertDir, id.String())
		err = os.Mkdir(filePath, 0755)
		if err != nil {
			logging.Error("Error creating directory: %s\n", err)
			return err
		}
		n.CertDir = filePath
		logging.Info(color.Red.Sprintf("Created directory: %s your certs are stored there if you want to keep them move them to a none temp directory", filePath))
	}

	kubePath, _ := kubernetes.ReadK8sConfigFromPath()
	kubeImpl, err := kubernetes.New(kubePath)
	if err != nil {
		logging.Error("Error creating kubeImpl: %s\n", err)
		return err
	}
	kube, err := kubernetes.NewClientFromFile(kubePath)
	if err != nil {
		logging.Error("Error creating kubeClient: %s\n", err)
		return err
	}

	n.Kube = kube
	n.KubeImpl = *kubeImpl

	return nil
}

func (n *NLXAddon) FilterAddons() {
	var filteredAddons FilteredAddons

	for _, subChart := range n.Addon.Charts {
		switch subChart.Name {
		case fmt.Sprintf("%s-%s", n.Addon.Namespace, strings.ToLower(INWAY)):
			filteredAddons.Inway = subChart
		case fmt.Sprintf("%s-%s", n.Addon.Namespace, strings.ToLower(OUTWAY)):
			filteredAddons.Outway = subChart
		case POSTGRES:
			filteredAddons.Postgres = subChart
		case NLXMANAGEMENT:
			filteredAddons.Management = subChart
		case CERTIFICATES:
			filteredAddons.CertManager = subChart
		default:
			continue
		}
	}

	n.FilteredAddons = filteredAddons
}

func (n *NLXAddon) CreateCertManager() error {
	installCertHelmChart := true
	// Detection via Pods.
	providerPods := []string{
		"cert-manager",
	}

	//an empty namespace is equal to all namespaces
	ns := ""
	pods, err := n.Kube.Pods().List(ns)
	if err != nil {
		return err
	}

	for _, pod := range pods.Items {
		for _, l := range providerPods {
			if strings.Contains(pod.Name, l) {
				installCertHelmChart = false
			}
		}
	}

	if installCertHelmChart {
		logging.Info("Cert Manager helm chart not found in your cluster: installing")

		err := n.ApplySubChart(n.FilteredAddons.CertManager)
		if err != nil {
			return err
		}

		return nil
	}

	providerCrdIssuer := "Issuer"
	providerCrdClusterIssuer := "ClusterIssuer"
	installIssuer := true
	installClusterIssuer := true

	crds, err := n.Kube.CustomResourceDefinitions().List()
	if err != nil {
		return err
	}

	for _, crd := range crds.Items {
		if strings.Contains(crd.Spec.Names.Kind, providerCrdClusterIssuer) {
			version := crd.Spec.Versions[len(crd.Spec.Versions)-1].Name
			rs, _ := n.Kube.CustomResources().Get(crd.Spec.Group, version, crd.Spec.Names.Plural, crd.Namespace, LETSENCRYPTNAME)
			if rs != nil {
				installClusterIssuer = false
			}
		}
		if strings.Contains(crd.Spec.Names.Kind, providerCrdIssuer) {
			version := crd.Spec.Versions[len(crd.Spec.Versions)-1].Name
			rs, _ := n.Kube.CustomResources().Get(crd.Spec.Group, version, crd.Spec.Names.Plural, crd.Namespace, INTERNALNAME)
			if rs != nil {
				installIssuer = false
			}
		}
	}

	if installIssuer {
		logging.Info("Cert Manager helm chart %s found in your cluster, skipping install", providerPods[0])
		logging.Info("Adding custom CRD to your cluster: %s", providerCrdIssuer)
		if len(n.FilteredAddons.CertManager.PostInstallYaml) > 0 {
			for _, y := range n.FilteredAddons.CertManager.PostInstallYaml {
				if y.Name == INTERNALNAME {
					if err := ApplySingleYaml(y, n.CustomValues, n.KubeImpl); err != nil {
						logging.Error(err.Error())
					}
				}
			}
		}
		if installClusterIssuer {
			logging.Info("Cert Manager helm chart %s found in your cluster, skipping install", providerPods[0])
			logging.Info("Adding custom CRD to your cluster: %s", providerCrdClusterIssuer)
			if len(n.FilteredAddons.CertManager.PostInstallYaml) > 0 {
				for _, y := range n.FilteredAddons.CertManager.PostInstallYaml {
					if y.Name == LETSENCRYPTNAME {
						if err := ApplySingleYaml(y, n.CustomValues, n.KubeImpl); err != nil {
							logging.Error(err.Error())
						}
					}
				}
			}
		}
	} else {
		// both cert manager and the issuer have been installed no action needed
		logging.Info("Cert Manager helm chart %s found in your cluster, skipping install", providerPods[0])
	}

	return nil

}

func (n *NLXAddon) CreateCertifications() error {
	logging.Info("Working on step 2: creating certificates")
	logging.Info("https://docs.nlx.io/try-nlx/helm/create-certificate")

	var certs NLXCerts

	crt, key, _ := certificates.GenerateKeyAndCertSet(n.CommonNameCerts)

	certs.CaKey = key
	certs.CaCert = crt

	if err := certificates.WriteKeyToFile(CAKEYNAME, n.CertDir, certs.CaKey); err != nil {
		logging.Error("Error writing %s to disk %s", CAKEYNAME, err)
	}

	if err := certificates.WriteKeyToFile(CACRTNAME, n.CertDir, certs.CaCert); err != nil {
		logging.Error("Error writing %s to disk %s", CACRTNAME, err)
	}

	logging.Info("Generated CA key and cert set")

	//create the tls secret, both tls.key and tls.crt are mandatory fields
	data := make(map[string][]byte)
	data["tls.key"] = certs.CaKey
	data["tls.crt"] = certs.CaCert

	err := n.Kube.Secrets().Create(n.Addon.Namespace, "internal-ca", "tls", data)
	if err != nil {
		logging.Error("Error creating secret: %s\n", err)
		return err
	}

	logging.Info("Creating external certs")

	certInfo := certificates.KeyGenerationInfo{
		CommonName:   n.CommonNameCerts,
		Country:      []string{"NL"},
		Province:     []string{"NLX"},
		Locality:     []string{"NLX"},
		Organization: []string{n.OrgName},
	}

	key, csr := certificates.GenerateExternalCertificate(certInfo)

	certs.OrgKey = key
	certs.OrgCsr = csr

	if err := certificates.WriteKeyToFile(ORGCSRMAME, n.CertDir, certs.OrgCsr); err != nil {
		logging.Error("Error writing %s to disk %s", ORGCSRMAME, err)
	}

	if err := certificates.WriteKeyToFile(ORGKEYNAME, n.CertDir, certs.OrgKey); err != nil {
		logging.Error("Error writing %s to disk %s", ORGKEYNAME, err)
	}

	values := map[string]string{"csr": string(certs.OrgCsr)}
	postData, _ := json.Marshal(values)
	u := url.URL{Scheme: SCHEME, Host: HOST, Path: REQUESTPATH}
	resp, err := handler.PostRequest(u, postData, "")
	if err != nil {
		logging.Error("Failed to make request to NLX: %s", err)
	}

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			logging.Error("Error closing response body %s", err)
		}
	}(resp.Body)

	var certificate ExternalCert
	err = json.NewDecoder(resp.Body).Decode(&certificate)
	if err != nil {
		logging.Error("Failed to parse json to ExternalCert: %s", err)
	}

	serialNumber, err := certificates.SerialNumberFromCert(certificate.Certificate)
	certs.OrgCert = []byte(certificate.Certificate)
	n.SerialNumber = serialNumber
	if err != nil {
		logging.Error("Failed to parse serial number from cert: %s", err)
	}

	if n.Outway {
		logging.Info(color.Green.Sprintf("Your org serial number is: %s."+
			" Save this, because it will be used later to access your own APIs when using the Outway.", n.SerialNumber))
	}

	if err := certificates.WriteKeyToFile(ORGCERTNAME, n.CertDir, certs.OrgCert); err != nil {
		logging.Error("Error writing %s to disk %s", ORGCERTNAME, err)
	}

	n.Certs = certs

	//create the tls secret, both tls.key and tls.crt are mandatory fields
	orgData := make(map[string][]byte)
	orgData["tls.key"] = certs.OrgKey
	orgData["tls.crt"] = certs.OrgCert

	err = n.Kube.Secrets().Create(n.Addon.Namespace, "external-tls", "tls", orgData)
	if err != nil {
		logging.Error("Error creating secret: %s\n", err)
		return err
	}

	return nil
}

func (n *NLXAddon) AddPostgres() error {
	logging.Info("Working on step 3: install PostgreSQL")
	logging.Info("https://docs.nlx.io/try-nlx/helm/postgresql")

	err := n.ApplySubChart(n.FilteredAddons.Postgres)
	if err != nil {
		return err
	}

	var password string
	secret, err := n.Kube.Secrets().Get(n.FilteredAddons.Postgres.Namespace, POSTGRES)
	if err != nil {
		logging.Error("Failed to create kube secret: %s", err)
	}
	for key, value := range secret.Data {
		if key == "postgresql-password" {
			password = string(value)
		}
	}

	if password == "" {
		return fmt.Errorf("no password found in secret %s", POSTGRES)
	}

	n.CustomValues["%POSTGRESPASSWORD%"] = password

	return nil
}

func (n *NLXAddon) ApplyNlxManagement() error {
	logging.Info("Working on step 4: install NLX Management")
	logging.Info("https://docs.nlx.io/try-nlx/helm/nlx-management")

	u := url.URL{Scheme: SCHEME, Host: HOST, Path: ROOTCERTNAME}
	logging.Info("Getting root cert from %s", u.String())
	resp, err := handler.GetRequest(u)
	if err != nil {
		logging.Error("Failed to make request to NLX: %s", err)
	}

	rootCert, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logging.Fatal(err.Error())
	}

	apiName := fmt.Sprintf("%s-api", n.FilteredAddons.Management.Name)
	n.CustomValues["%APIDNSNAME%"] = apiName

	indentedRootCert := n.Addon.AddIndentation(string(rootCert), 6)
	indentedOrgCert := n.Addon.AddIndentation(string(n.Certs.OrgCert), 6)
	indentedOrgKey := n.Addon.AddIndentation(string(n.Certs.OrgKey), 6)
	indentedCaCert := n.Addon.AddIndentation(string(n.Certs.CaCert), 6)

	n.CustomValues["%ROOTCRT%"] = indentedRootCert
	n.CustomValues["%ORGCRT%"] = indentedOrgCert
	n.CustomValues["%ORGKEY%"] = indentedOrgKey
	n.CustomValues["%CACRT%"] = indentedCaCert

	err = n.ApplySubChart(n.FilteredAddons.Management)
	if err != nil {
		return err
	}

	logging.Info(color.Green.Sprintf("Created new user: %s", n.CustomValues["%ADMINUSER%"]))

	uiName := fmt.Sprintf("%s-ui", n.FilteredAddons.Management.Name)

	var port int32
	service, _ := n.Kube.Services().Get(n.Addon.Namespace, apiName)
	for _, p := range service.Spec.Ports {
		if p.Name == "https" {
			port = p.Port
		}
	}
	n.CustomValues["%MANAGEMENTAPI%"] = fmt.Sprintf("%s:%d", apiName, port)

	logging.Info(color.Green.Sprintf("You can use this user to log into the management UI:"))
	logging.Info(color.Green.Sprintf("$ kubectl -n nlx port-forward svc/%s 8080:8080", uiName))
	logging.Info(color.Green.Sprintf("The ui will then be available with your browser at: http://localhost:8080"))

	return nil
}

func (n *NLXAddon) ApplyInway() error {
	logging.Info("Working on step 5: install Inway (optional step)")
	logging.Info("https://docs.nlx.io/try-nlx/helm/nlx-inway")

	n.CustomValues["%INWAYNAME%"] = fmt.Sprintf("%s-%s", n.FilteredAddons.Inway.Name, n.FilteredAddons.Inway.Namespace)

	err := n.ApplySubChart(n.FilteredAddons.Inway)
	if err != nil {
		return err
	}

	return nil
}

func (n *NLXAddon) ApplyOutway() error {
	logging.Info("Working on step 6: install Outway (optional step)")
	logging.Info("https://docs.nlx.io/try-nlx/helm/nlx-outway")

	err := n.ApplySubChart(n.FilteredAddons.Outway)
	if err != nil {
		return err
	}

	return nil
}

func (n *NLXAddon) ApplySubChart(subChart NestedChart) error {
	helmClient, err := helm.NewClient(subChart.Namespace, &n.KubeImpl)
	if err != nil {
		logging.Error("Error constructing client: %s\n", err)
		return err
	}

	n.Addon.DefaultValuesFile = subChart.DefaultValuesFile

	values, err := n.Addon.GetValues(n.CustomValues)
	if err != nil {
		logging.Error("Error loading values: %s\n", err)
		return err
	}

	if len(subChart.PreInstallYaml) > 0 {
		if err := ApplyYaml(subChart.PreInstallYaml, n.CustomValues, n.KubeImpl); err != nil {
			logging.Error(err.Error())
		}
	}

	_, err = helmClient.AddRepository(subChart.Repository.Name, subChart.Repository.URL)
	if err != nil {
		logging.Error("Error adding repository: %s\n", err)
	}

	chartName := fmt.Sprintf("%s/%s", subChart.Repository.Name, subChart.Name)

	if subChart.Repository.Version == "" {
		logging.Info("Helm installing %s in namespace %s (may take a few moments)", subChart.Name, subChart.Namespace)
		_, err = helmClient.Install(
			subChart.Name,
			chartName,
			values,
		)
	} else {
		logging.Info("Helm installing %s in namespace %s with version %s (may take a few moments)", subChart.Name, subChart.Namespace, subChart.Repository.Version)
		_, err = helmClient.InstallSpecificVersion(
			subChart.Name,
			chartName,
			subChart.Repository.Version,
			values,
		)
	}

	if err != nil {
		logging.Error("Error installing release: %s\n", err)
		return err
	}

	if len(subChart.PostInstallYaml) > 0 {
		if err := ApplyYaml(subChart.PostInstallYaml, n.CustomValues, n.KubeImpl); err != nil {
			logging.Error(err.Error())
		}
	}

	logging.Info("Installed subChart %s in namespace %s\n", subChart.Name, subChart.Namespace)

	return nil
}

func determineTextBaseFlags(key string, value interface{}) (createResource bool) {
	if value == "no" {
		createResource = false
	} else if value == "yes" {
		createResource = true
	} else {
		logging.Info("Unknown value for %s expected yes or no got %s", key, value)
		logging.Info("Invalid input assuming yes")
		createResource = true
	}

	return
}
