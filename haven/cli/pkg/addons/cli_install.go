// Copyright © VNG Realisatie 2019-2022
// Licensed under EUPL v1.2

package addons

import (
	"fmt"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes"

	"github.com/AlecAivazis/survey/v2"
	cli "github.com/jawher/mow.cli"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/helm"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/rand"
)

func cmdInstall(cmd *cli.Cmd) {
	name := cmd.StringArg("NAME", "", "The addon to install")

	cmd.Action = func() {
		addon, err := Get(*name)

		if err != nil {
			logging.Error("Error while getting addon: %s\n", err)
			return
		}

		//we have a separate flow for nlx since that requires multiple helm installs
		if *name == "nlx" {
			nlx := NLXAddon{Addon: *addon}
			nlx.InstallNLXHelmCharts()
			return
		}

		kubePath, _ := kubernetes.ReadK8sConfigFromPath()
		kube, err := kubernetes.New(kubePath)
		if err != nil {
			logging.Error("Error creating kubeClient: %s\n", err)
			return
		}

		helmClient, err := helm.NewClient(addon.Namespace, kube)
		if err != nil {
			logging.Error("Error constructing client: %s\n", err)
			return
		}

		customValues := map[string]interface{}{}
		err = survey.Ask(addon.GetSurveyQuestions(), &customValues)
		if err != nil {
			logging.Error("Error processing answers: %s\n", err)
			return
		}

		for _, generatedValue := range addon.GeneratedValues {
			customValues[generatedValue.Name] = rand.String(generatedValue.RandString)
		}

		if len(addon.PreInstallYaml) > 0 {
			if err := ApplyYaml(addon.PreInstallYaml, customValues, *kube); err != nil {
				logging.Error(err.Error())
			}
		}

		logging.Info("Adding repository %s\n", addon.Chart.Repository.Name)

		_, err = helmClient.AddRepository(addon.Chart.Repository.Name, addon.Chart.Repository.URL)
		if err != nil {
			logging.Error("Error adding repository: %s\n", err)
		}

		logging.Info("Installing %s\n", addon.Name)

		values, err := addon.GetValues(customValues)
		if err != nil {
			logging.Error("Error getting default values: %s\n", err)
			return
		}

		_, err = helmClient.Install(
			addon.Name,
			fmt.Sprintf("%s/%s", addon.Chart.Repository.Name, addon.Chart.Name),
			values,
		)

		if err != nil {
			logging.Error("Error installing release: %s\n", err)
			return
		}

		if len(addon.PostInstallYaml) > 0 {
			if err := ApplyYaml(addon.PostInstallYaml, customValues, *kube); err != nil {
				logging.Error(err.Error())
			}
		}

		logging.Info("Installed addon %s in namespace %s\n", addon.Name, addon.Namespace)
	}
}
