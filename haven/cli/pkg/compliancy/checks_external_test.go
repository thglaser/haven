package compliancy

import (
	"encoding/base64"
	"github.com/Masterminds/semver/v3"
	"github.com/stretchr/testify/assert"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/sonobuoy"
	"strconv"
	"strings"
	"testing"
	"time"
)

func TestSuggestionExternalCIS(t *testing.T) {
	version := "v1.21.5"
	ns := "test"
	appName := "kube-bench-" + strings.ToLower(base64.StdEncoding.EncodeToString([]byte(strconv.Itoa(time.Now().Nanosecond())))[:5])
	globalInterval := 1 * time.Millisecond
	testClient, err := kubernetes.FakeKubeClient()
	assert.Nil(t, err)

	kubeServer, err := semver.NewVersion(version)
	assert.Nil(t, err)
	err = kubernetes.CreatePodForTest(appName, ns, testClient)
	assert.Nil(t, err)

	t.Run("PlatformUnknown", func(t *testing.T) {
		config := Config{
			Kube:            testClient,
			HostPlatform:    PlatformUnknown,
			KubeServer:      kubeServer,
			CisCheckAppName: appName,
			GlobalInterval:  globalInterval,
		}

		result, err := suggestionExternalCIS(&config)
		assert.Nil(t, err)
		// since the logs are always "fake LOGS" the result will always be NO
		assert.Equal(t, result, ResultNo)
	})

	t.Run("PlatformGKE", func(t *testing.T) {
		config := Config{
			Kube:            testClient,
			HostPlatform:    PlatformGKE,
			KubeServer:      kubeServer,
			CisCheckAppName: appName,
			GlobalInterval:  globalInterval,
		}

		result, err := suggestionExternalCIS(&config)
		assert.Nil(t, err)
		// since the logs are always "fake LOGS" the result will always be NO
		assert.Equal(t, result, ResultNo)
	})

	t.Run("PlatformEKS", func(t *testing.T) {
		config := Config{
			Kube:            testClient,
			HostPlatform:    PlatformEKS,
			KubeServer:      kubeServer,
			CisCheckAppName: appName,
			GlobalInterval:  globalInterval,
		}

		result, err := suggestionExternalCIS(&config)
		assert.Nil(t, err)
		// since the logs are always "fake LOGS" the result will always be NO
		assert.Equal(t, result, ResultNo)
	})

	t.Run("PlatformAKS", func(t *testing.T) {
		config := Config{
			Kube:            testClient,
			HostPlatform:    PlatformAKS,
			KubeServer:      kubeServer,
			CisCheckAppName: appName,
			GlobalInterval:  globalInterval,
		}

		result, err := suggestionExternalCIS(&config)
		assert.Nil(t, err)
		// since the logs are always "fake LOGS" the result will always be NO
		assert.Equal(t, result, ResultNo)
	})

}

func TestExternalCNCF(t *testing.T) {
	standardError := sonobuoy.DefaultError

	t.Run("HappyFlow", func(t *testing.T) {
		sonoTestClient := sonobuoy.NewMockSonobuoyClient(2, 100, 0, "", "")
		config := Config{
			Sono:           sonoTestClient,
			HostPlatform:   PlatformUnknown,
			GlobalInterval: 2 * time.Millisecond,
		}

		result, err := externalCNCF(&config)
		assert.Nil(t, err)
		assert.Equal(t, ResultYes, result)
	})

	t.Run("HappyFlowWithFailedTest", func(t *testing.T) {
		sonoTestClient := sonobuoy.NewMockSonobuoyClient(1, 2, 0, "failed", "")
		config := Config{
			Sono:           sonoTestClient,
			HostPlatform:   PlatformUnknown,
			GlobalInterval: 2 * time.Millisecond,
		}

		result, err := externalCNCF(&config)
		assert.Nil(t, err)
		assert.Equal(t, ResultNo, result)
	})

	t.Run("FailingRun", func(t *testing.T) {
		sonoTestClient := sonobuoy.NewMockSonobuoyClient(1, 2, 0, "failed", "Run")
		config := Config{
			Sono:           sonoTestClient,
			HostPlatform:   PlatformUnknown,
			GlobalInterval: 2 * time.Millisecond,
		}

		result, err := externalCNCF(&config)
		assert.NotNil(t, err)
		assert.Equal(t, ResultNo, result)
		assert.Contains(t, err.Error(), standardError)
		assert.Contains(t, err.Error(), "run")
	})

	t.Run("FailingStatus", func(t *testing.T) {
		sonoTestClient := sonobuoy.NewMockSonobuoyClient(1, 2, 1, "failed", "")
		config := Config{
			Sono:           sonoTestClient,
			HostPlatform:   PlatformUnknown,
			GlobalInterval: 2 * time.Millisecond,
		}

		result, err := externalCNCF(&config)
		assert.Nil(t, err)
		assert.Equal(t, ResultNo, result)
	})

	t.Run("FailingRetrieve", func(t *testing.T) {
		sonoTestClient := sonobuoy.NewMockSonobuoyClient(1, 2, 0, "", "")
		config := Config{
			Sono:           sonoTestClient,
			HostPlatform:   PlatformUnknown,
			GlobalInterval: 2 * time.Millisecond,
		}

		result, err := externalCNCF(&config)
		assert.Nil(t, err)
		assert.Equal(t, ResultYes, result)
	})

	t.Run("FailingResults", func(t *testing.T) {
		sonoTestClient := sonobuoy.NewMockSonobuoyClient(1, 2, 0, "failed", "Results")
		config := Config{
			Sono:           sonoTestClient,
			HostPlatform:   PlatformUnknown,
			GlobalInterval: 2 * time.Millisecond,
		}

		result, err := externalCNCF(&config)
		assert.NotNil(t, err)
		assert.Equal(t, ResultNo, result)
		assert.Contains(t, err.Error(), standardError)
		assert.Contains(t, err.Error(), "results")
	})

	t.Run("FailingDelete", func(t *testing.T) {
		sonoTestClient := sonobuoy.NewMockSonobuoyClient(1, 2, 0, "failed", "Delete")
		config := Config{
			Sono:           sonoTestClient,
			HostPlatform:   PlatformUnknown,
			GlobalInterval: 2 * time.Millisecond,
		}

		result, err := externalCNCF(&config)
		assert.NotNil(t, err)
		assert.Equal(t, ResultNo, result)
		assert.Contains(t, err.Error(), standardError)
		assert.Contains(t, err.Error(), "delete")
	})
}
