package compliancy

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes"
	corev1 "k8s.io/api/core/v1"
)

func TestPod(t *testing.T) {
	ns := "cmdonpod-test"
	cmd := "unittests-cmdonpod_test.go"
	nodeName := "someNodeName"
	globalInterval := 1 * time.Millisecond
	testClient, err := kubernetes.FakeKubeClient()
	assert.Nil(t, err)
	errorText := "pod"
	staticTextFromExecPod := "FakeClientDoesNotExecute"

	t.Run("RunWithoutMaster", func(t *testing.T) {
		config := Config{
			Kube:           testClient,
			GlobalInterval: globalInterval,
			Namespace:      ns,
		}

		done := make(chan struct{})

		go func() {
			err := kubernetes.SetPodStatusToRunning(done, testClient, ns)
			assert.Nil(t, err)
		}()
		sut, err := RunCommandOnPod(&config, false, cmd)
		close(done)
		assert.Nil(t, err)
		assert.Contains(t, sut, staticTextFromExecPod)
	})

	t.Run("CannotFindAnyMasterNode", func(t *testing.T) {
		config := Config{
			Kube:           testClient,
			GlobalInterval: globalInterval,
		}

		sut, err := RunCommandOnPod(&config, true, cmd)
		assert.NotNil(t, err)
		assert.Equal(t, "", sut)
		assert.Contains(t, err.Error(), errorText)
	})

	t.Run("MasterNodeFound", func(t *testing.T) {
		testClient, err = kubernetes.FakeKubeClient()
		assert.Nil(t, err)

		config := Config{
			Kube:           testClient,
			GlobalInterval: globalInterval,
			Namespace:      ns,
		}

		taints := []corev1.Taint{
			{
				Key:       "testKey",
				Value:     "testValue",
				Effect:    "testEffect",
				TimeAdded: nil,
			},
		}

		labels := map[string]string{"kubernetes.io/role": "master"}
		err = kubernetes.CreateNodeForTest(nodeName, taints, labels, testClient)
		assert.Nil(t, err)

		done := make(chan struct{})

		go func() {
			err := kubernetes.SetPodStatusToRunning(done, testClient, ns)
			assert.Nil(t, err)
		}()
		sut, err := RunCommandOnPod(&config, true, cmd)
		close(done)
		assert.Nil(t, err)
		assert.Contains(t, sut, staticTextFromExecPod)
	})

	t.Run("MasterNodeFoundLabelMaster", func(t *testing.T) {
		testClient, err = kubernetes.FakeKubeClient()
		assert.Nil(t, err)

		config := Config{
			Kube:           testClient,
			GlobalInterval: globalInterval,
			Namespace:      ns,
		}

		taints := []corev1.Taint{
			{
				Key:       "testKey",
				Value:     "testValue",
				Effect:    "testEffect",
				TimeAdded: nil,
			},
		}

		labels := map[string]string{"node-role.kubernetes.io/master": "exists"}
		err = kubernetes.CreateNodeForTest(nodeName, taints, labels, testClient)
		assert.Nil(t, err)

		done := make(chan struct{})

		go func() {
			err := kubernetes.SetPodStatusToRunning(done, testClient, ns)
			assert.Nil(t, err)
		}()
		sut, err := RunCommandOnPod(&config, true, cmd)
		close(done)
		assert.Nil(t, err)
		assert.Contains(t, sut, staticTextFromExecPod)
	})

	t.Run("MasterNodeFoundLabelControlPlane", func(t *testing.T) {
		testClient, err = kubernetes.FakeKubeClient()
		assert.Nil(t, err)

		config := Config{
			Kube:           testClient,
			GlobalInterval: globalInterval,
			Namespace:      ns,
		}

		taints := []corev1.Taint{
			{
				Key:       "testKey",
				Value:     "testValue",
				Effect:    "testEffect",
				TimeAdded: nil,
			},
		}

		labels := map[string]string{"node-role.kubernetes.io/controlplane": "exists"}
		err = kubernetes.CreateNodeForTest(nodeName, taints, labels, testClient)
		assert.Nil(t, err)

		done := make(chan struct{})

		go func() {
			err := kubernetes.SetPodStatusToRunning(done, testClient, ns)
			assert.Nil(t, err)
		}()
		sut, err := RunCommandOnPod(&config, true, cmd)
		close(done)
		assert.Nil(t, err)
		assert.Contains(t, sut, staticTextFromExecPod)
	})
}
