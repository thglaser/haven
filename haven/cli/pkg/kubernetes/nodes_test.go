package kubernetes

import (
	"github.com/stretchr/testify/assert"
	corev1 "k8s.io/api/core/v1"
	"testing"
)

func TestNodesClient(t *testing.T) {
	expectedName := "testnode"

	t.Run("List", func(t *testing.T) {
		testClient, err := FakeKubeClient()
		assert.Nil(t, err)

		nodes, err := testClient.Nodes().List()
		assert.Nil(t, err)
		assert.True(t, len(nodes.Items) == 0)
	})

	t.Run("Create", func(t *testing.T) {
		testClient, err := FakeKubeClient()
		assert.Nil(t, err)
		taints := []corev1.Taint{{
			Key:       "",
			Value:     "",
			Effect:    "",
			TimeAdded: nil,
		}}

		nodeObject := createNodeItem(expectedName, taints, nil)

		node, err := testClient.Nodes().Create(nodeObject)
		assert.Nil(t, err)
		assert.Equal(t, expectedName, node.Name)
	})

	t.Run("CreateAndList", func(t *testing.T) {
		testClient, err := FakeKubeClient()
		assert.Nil(t, err)
		taints := []corev1.Taint{{
			Key:       "",
			Value:     "",
			Effect:    "",
			TimeAdded: nil,
		}}

		nodeObject := createNodeItem(expectedName, taints, nil)

		node, err := testClient.Nodes().Create(nodeObject)
		assert.Nil(t, err)
		assert.Equal(t, expectedName, node.Name)

		nodes, err := testClient.Nodes().List()
		assert.Nil(t, err)
		assert.True(t, len(nodes.Items) == 1)
	})
}
