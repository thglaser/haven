package haven

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes"
	"testing"
)

func TestIntegrationDefinitionNotCreatedIfExists(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	kubeConfig, err := kubernetes.ReadK8sConfigFromPathWithoutLogging()
	assert.Nil(t, err)

	crdClient, err := NewCrdConfig(kubeConfig)
	assert.Nil(t, err)

	_, err = crdClient.CreateHavenDefinition()

	assert.Nil(t, err)
}

func TestIntegrationDefinitionCreatedIfNotExists(t *testing.T) {
	if testing.Short() {
		t.Skip()
	}

	kubeConfig, err := kubernetes.ReadK8sConfigFromPathWithoutLogging()
	assert.Nil(t, err)

	crdClient, err := NewCrdConfig(kubeConfig)
	assert.Nil(t, err)

	_, err = crdClient.CreateHavenDefinition()

	assert.Nil(t, err)
}
