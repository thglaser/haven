package kubernetes

import (
	"context"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
	corev1 "k8s.io/api/core/v1"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	v1 "k8s.io/client-go/kubernetes/typed/core/v1"
	"time"
)

type SecretImpl struct {
	client v1.CoreV1Interface
}

// NewSecretClient to interact with Secret interface
func NewSecretClient(kube kubernetes.Interface) (*SecretImpl, error) {
	coreClient := kube.CoreV1()

	return &SecretImpl{client: coreClient}, nil
}

// Create a secret in a namespace in your cluster
// typeAsString should be a secret type and defaults to opaque
func (s *SecretImpl) Create(namespace, secretName, typeAsString string, data map[string][]byte) error {
	var secretType corev1.SecretType

	switch typeAsString {
	case "tls":
		secretType = corev1.SecretTypeTLS
	case "docker":
		secretType = corev1.SecretTypeDockercfg
	default:
		secretType = corev1.SecretTypeOpaque
	}

	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	_, err := s.Get(namespace, secretName)

	if err != nil {
		if k8serrors.IsNotFound(err) {
			logging.Info("Secret %s not found, creating", secretName)
			secret := corev1.Secret{
				TypeMeta: metav1.TypeMeta{
					Kind:       "Secret",
					APIVersion: "v1",
				},
				ObjectMeta: metav1.ObjectMeta{
					Name: secretName,
				},
				Immutable:  nil,
				Data:       data,
				StringData: nil,
				Type:       secretType,
			}
			_, err := s.client.Secrets(namespace).Create(ctx, &secret, metav1.CreateOptions{})
			if err != nil {
				return err
			}

			logging.Info("Created secret %s in namespace %s", secretName, namespace)
		}
	} else {
		logging.Info("Secret %s already exists in namespace %s", secretName, namespace)
	}

	return nil
}

// Get a secrets in a namespace in your kube cluster
func (s *SecretImpl) Get(namespace, secretName string) (*corev1.Secret, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	secret, err := s.client.Secrets(namespace).Get(ctx, secretName, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}

	return secret, nil
}

// List all secrets in a namespace in your kube cluster
func (s *SecretImpl) List(namespace string) (*corev1.SecretList, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	secrets, err := s.client.Secrets(namespace).List(ctx, metav1.ListOptions{})
	if err != nil {
		return nil, err
	}

	return secrets, nil
}

// Delete removes a secret from your kube cluster
func (s *SecretImpl) Delete(namespace, secretName string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	err := s.client.Secrets(namespace).Delete(ctx, secretName, metav1.DeleteOptions{})

	return err
}
