package kubernetes

import (
	"context"
	"fmt"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/rest"
)

type CustomResourceImpl struct {
	rest *rest.Config
}

// NewCRClient to interact with CustomResource interface
func NewCRClient(kube *rest.Config) (*CustomResourceImpl, error) {
	return &CustomResourceImpl{rest: kube}, nil
}

// Delete deletes a named customresource
func (c *CustomResourceImpl) Delete(group, version, resource, namespace, name string) error {
	d, err := dynamic.NewForConfig(c.rest)
	if err != nil {
		return fmt.Errorf("Could not create dynamic Kubernetes client: %s", err.Error())
	}

	err = d.Resource(schema.GroupVersionResource{Group: group, Version: version, Resource: resource}).Namespace(namespace).Delete(context.Background(), name, metav1.DeleteOptions{})
	return err
}

// Get gets a named customresource
func (c *CustomResourceImpl) Get(group, version, resource, namespace, name string) (*unstructured.Unstructured, error) {
	d, err := dynamic.NewForConfig(c.rest)
	if err != nil {
		return nil, fmt.Errorf("Could not create dynamic Kubernetes client: %s", err.Error())
	}

	rsc, err := d.Resource(schema.GroupVersionResource{Group: group, Version: version, Resource: resource}).Namespace(namespace).Get(context.Background(), name, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}

	return rsc, nil
}

// List lists customresources
func (c *CustomResourceImpl) List(group, version, resource string) (*unstructured.UnstructuredList, error) {
	d, err := dynamic.NewForConfig(c.rest)
	if err != nil {
		return nil, fmt.Errorf("Could not create dynamic Kubernetes client: %s", err.Error())
	}

	resources, err := d.Resource(schema.GroupVersionResource{Group: group, Version: version, Resource: resource}).List(context.Background(), metav1.ListOptions{})
	if err != nil {
		return nil, err
	}

	return resources, nil
}
