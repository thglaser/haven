package kubernetes

import (
	"context"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	v1 "k8s.io/client-go/kubernetes/typed/core/v1"
	"time"
)

type PvcImpl struct {
	client v1.CoreV1Interface
}

// NewVolumeClaimClient to interact with VolumeClaim interface
func NewVolumeClaimClient(kube kubernetes.Interface) (*PvcImpl, error) {
	coreClient := kube.CoreV1()

	return &PvcImpl{client: coreClient}, nil
}

// List gets a list of volumeclaim from a ns - empty string will be treated as all
func (p *PvcImpl) List(namespace string) (*corev1.PersistentVolumeClaimList, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	pvcs, err := p.client.PersistentVolumeClaims(namespace).List(ctx, metav1.ListOptions{})
	if err != nil {
		return nil, err
	}

	return pvcs, nil
}

// Get gets a single volumeclaim
func (p *PvcImpl) Get(namespace, name string) (*corev1.PersistentVolumeClaim, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	pvcs, err := p.client.PersistentVolumeClaims(namespace).Get(ctx, name, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}

	return pvcs, nil
}

// Delete deletes a named volume claim
func (p *PvcImpl) Delete(namespace, name string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	err := p.client.PersistentVolumeClaims(namespace).Delete(ctx, name, metav1.DeleteOptions{})
	return err
}

// Create creates a named volume claim
func (p *PvcImpl) Create(namespace string, pvc *corev1.PersistentVolumeClaim) (*corev1.PersistentVolumeClaim, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	pvc, err := p.client.PersistentVolumeClaims(namespace).Create(ctx, pvc, metav1.CreateOptions{})
	return pvc, err
}
