package kubernetes

import (
	"context"
	extv1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	extensionsclient "k8s.io/apiextensions-apiserver/pkg/client/clientset/clientset"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"time"
)

type CustomResourceDefinitionImpl struct {
	client extensionsclient.Interface
}

// NewCRDClient to interact with CustomResourceDefinition interface
func NewCRDClient(kube extensionsclient.Interface) (*CustomResourceDefinitionImpl, error) {
	return &CustomResourceDefinitionImpl{client: kube}, nil
}

// Create creates the CRDs within a cluster
func (c *CustomResourceDefinitionImpl) Create(crd *extv1.CustomResourceDefinition) (*extv1.CustomResourceDefinition, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Minute)
	defer cancel()

	crd, err := c.client.ApiextensionsV1().CustomResourceDefinitions().Create(ctx, crd, metav1.CreateOptions{})
	if err != nil {
		return nil, err
	}

	return crd, nil
}

// List lists the CRDs within a cluster
func (c *CustomResourceDefinitionImpl) List() (*extv1.CustomResourceDefinitionList, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Minute)
	defer cancel()

	crds, err := c.client.ApiextensionsV1().CustomResourceDefinitions().List(ctx, metav1.ListOptions{})
	if err != nil {
		return nil, err
	}

	return crds, nil
}

// Delete deletes the CRDs within a cluster
func (c *CustomResourceDefinitionImpl) Delete(name string) error {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	err := c.client.ApiextensionsV1().CustomResourceDefinitions().Delete(ctx, name, metav1.DeleteOptions{})

	return err
}
