package kubernetes

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestPodsClient(t *testing.T) {
	t.Run("Create", func(t *testing.T) {
		testClient, err := FakeKubeClient()
		assert.Nil(t, err)
		expectedName := "testpod"
		ns := "test"

		podObject := createPodObject(expectedName, ns)

		pod, err := testClient.Pods().Create(ns, podObject)
		assert.Nil(t, err)
		assert.Equal(t, expectedName, pod.Name)
		assert.Equal(t, ns, pod.Namespace)
	})

	t.Run("CreatingTwiceReturnsError", func(t *testing.T) {
		testClient, err := FakeKubeClient()
		assert.Nil(t, err)
		expectedName := "testpod"
		ns := "test"

		podObject := createPodObject(expectedName, ns)

		_, err = testClient.Pods().Create(ns, podObject)
		assert.Nil(t, err)
		_, err = testClient.Pods().Create(ns, podObject)
		assert.NotNil(t, err)
		assert.Contains(t, err.Error(), expectedName)
	})

	t.Run("Get", func(t *testing.T) {
		testClient, err := FakeKubeClient()
		assert.Nil(t, err)
		expectedName := "testpod"
		assert.Nil(t, err)
		ns := "test"

		// we need to create a pod first
		err = CreatePodForTest(expectedName, ns, testClient)
		assert.Nil(t, err)

		pod, err := testClient.Pods().Get(ns, expectedName)
		assert.Nil(t, err)
		assert.Equal(t, expectedName, pod.Name)
		assert.Equal(t, ns, pod.Namespace)
	})

	t.Run("GetByLabel", func(t *testing.T) {
		testClient, err := FakeKubeClient()
		assert.Nil(t, err)
		expectedName := "testpod"
		assert.Nil(t, err)
		ns := "test"

		// we need to create a pod first
		err = CreatePodForTest(expectedName, ns, testClient)
		assert.Nil(t, err)

		label := fmt.Sprintf("app=%s", expectedName)
		pods, err := testClient.Pods().GetByLabel(ns, label)
		assert.Nil(t, err)
		assert.True(t, len(pods.Items) == 1)
		assert.Equal(t, expectedName, pods.Items[0].Name)
		assert.Equal(t, ns, pods.Items[0].Namespace)
	})

	t.Run("List", func(t *testing.T) {
		testClient, err := FakeKubeClient()
		assert.Nil(t, err)
		expected := "testpod"
		expectedNamePodOne := fmt.Sprintf("%sone", expected)
		expectedNamePodTwo := fmt.Sprintf("%stwo", expected)
		ns := "test"

		// we need to create a pod first
		err = CreatePodForTest(expectedNamePodOne, ns, testClient)
		assert.Nil(t, err)
		err = CreatePodForTest(expectedNamePodTwo, ns, testClient)
		assert.Nil(t, err)

		pods, err := testClient.Pods().List(ns)
		assert.Nil(t, err)
		assert.Equal(t, 2, len(pods.Items))
		for _, pod := range pods.Items {
			assert.Equal(t, pod.Namespace, ns)
			assert.Contains(t, pod.Name, expected)

		}
	})

	t.Run("Logs", func(t *testing.T) {
		testClient, err := FakeKubeClient()
		assert.Nil(t, err)
		podName := "testpod"
		expected := "fake logs"
		ns := "test"

		// we need to create a pod first
		err = CreatePodForTest(podName, ns, testClient)
		assert.Nil(t, err)

		logs, err := testClient.Pods().Logs(ns, podName)
		assert.Nil(t, err)
		assert.Equal(t, expected, logs)
	})

	t.Run("Exec", func(t *testing.T) {
		testClient, err := FakeKubeClient()
		assert.Nil(t, err)
		expectedName := "testpod"
		command := []string{"ls", "-lha"}
		assert.Nil(t, err)
		ns := "test"

		text, err := testClient.Pods().ExecNamed(ns, expectedName, command)
		assert.Nil(t, err)
		assert.Equal(t, text, "FakeClientDoesNotExecute")
	})
}
