package main

import "gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes/models/crds/haven/v1alpha"

//go:generate go run generator.go
func main() {
	v1alpha.Generate()
}
