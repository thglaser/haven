package kubernetes

import (
	"context"
	rbacv1 "k8s.io/api/rbac/v1"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/typed/rbac/v1"
	"time"
)

type RolesImpl struct {
	rbacClient v1.RbacV1Interface
}

// NewRolesClient to interact with Roles interface
func NewRolesClient(kube kubernetes.Interface) (*RolesImpl, error) {
	clientSet := kube.RbacV1()

	return &RolesImpl{rbacClient: clientSet}, nil
}

//CreateClusterRole creates a clusterrole if it does not already exist
func (r *RolesImpl) CreateClusterRole(clusterRole *rbacv1.ClusterRole) error {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	_, err := r.GetClusterRole(clusterRole.Name)
	if err != nil {
		if k8serrors.IsNotFound(err) {
			_, err := r.rbacClient.ClusterRoles().Create(ctx, clusterRole, metav1.CreateOptions{})
			if err != nil {
				return err
			}

			return nil
		}
	}

	return nil
}

//GetClusterRole get a clusterrole
func (r *RolesImpl) GetClusterRole(name string) (*rbacv1.ClusterRole, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	role, err := r.rbacClient.ClusterRoles().Get(ctx, name, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}

	return role, nil
}

//CreateClusterRoleBinding creates a clusterrole binding if it does not already exist
func (r *RolesImpl) CreateClusterRoleBinding(clusterRoleBinding *rbacv1.ClusterRoleBinding) error {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	_, err := r.GetClusterRoleBinding(clusterRoleBinding.Name)
	if err != nil {
		if k8serrors.IsNotFound(err) {
			_, err := r.rbacClient.ClusterRoleBindings().Create(ctx, clusterRoleBinding, metav1.CreateOptions{})
			if err != nil {
				return err
			}

			return nil
		}
	}

	return nil

}

//GetClusterRoleBinding get a clusterrolebinding
func (r *RolesImpl) GetClusterRoleBinding(name string) (*rbacv1.ClusterRoleBinding, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()

	binding, err := r.rbacClient.ClusterRoleBindings().Get(ctx, name, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}

	return binding, nil
}
