variable "group_name" {
  description = "AKS admins AAD Group name"
  type        = string
  default     = null
}