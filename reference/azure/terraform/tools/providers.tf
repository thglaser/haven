provider "azurerm" {
  features {}
}

provider "kubernetes" {
  alias       = "aks"
  config_path = var.kubeconfig
}

provider "helm" {
  alias = "aks"
  kubernetes {
    config_path = var.kubeconfig
  }
}
