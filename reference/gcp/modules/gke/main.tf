#   Copyright 2021 Google LLC
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

terraform {
  required_version = ">= 1.0.0"

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 3.89.0"
    }
    google-beta = {
      source  = "hashicorp/google-beta"
      version = ">= 3.89.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.1.0"
    }
  }
}

locals {
  gke_secrets_service_account = format("service-%d@container-engine-robot.iam.gserviceaccount.com", var.project_number)
  gke_nodes_service_account   = format("service-%d@compute-system.iam.gserviceaccount.com", var.project_number)
}

resource "random_id" "kms" {
  count       = var.use_cmek ? 1 : 0
  byte_length = 4
}

# Create a Cloud KMS keyring for Application-level Secrets Encryption
module "kms" {
  count = var.use_cmek ? 1 : 0

  source = "github.com/terraform-google-modules/cloud-foundation-fabric//modules/kms?ref=v8.0.0"

  project_id = var.project_id
  iam = {
    "roles/cloudkms.cryptoKeyEncrypterDecrypter" = [
      format("serviceAccount:%s", local.gke_secrets_service_account),
      format("serviceAccount:%s", local.gke_nodes_service_account)
    ]
  }
  keyring        = { location = var.region, name = format("%s-keyring-%s", var.cluster_name, random_id.kms[0].hex) }
  keyring_create = true
  keys           = { gke-nodes = null, gke-secrets = null }
}

# Create a service account for the GKE cluster
module "gke-service-account" {
  source = "github.com/terraform-google-modules/cloud-foundation-fabric//modules/iam-service-account?ref=v8.0.0"

  project_id   = var.project_id
  name         = format("%s-sa", var.cluster_name)
  generate_key = false

  iam_project_roles = {
    (var.project_id) = var.service_account_roles
  }
}

# Create a GKE cluster
module "gke" {
  source = "github.com/terraform-google-modules/cloud-foundation-fabric//modules/gke-cluster?ref=v8.0.0"

  project_id      = var.project_id
  name            = var.cluster_name
  location        = var.region
  release_channel = "REGULAR"

  network                  = var.gke_network
  subnetwork               = var.gke_subnet
  secondary_range_pods     = var.ip_range_pods
  secondary_range_services = var.ip_range_services

  enable_shielded_nodes = true
  database_encryption = {
    enabled  = var.use_cmek
    state    = "ENCRYPTED"
    key_name = var.use_cmek ? module.kms[0].key_self_links.gke-secrets : null
  }
  enable_binary_authorization = var.enable_binary_authorization

  default_max_pods_per_node = var.max_pods_per_node
  # Dataplane V2 automatically enables network policy
  enable_dataplane_v2      = true
  enable_l4_ilb_subsetting = var.enable_l4_ilb_subsetting
  addons = {
    http_load_balancing                   = true
    gce_persistent_disk_csi_driver_config = true

    dns_cache_config           = false
    cloudrun_config            = false
    horizontal_pod_autoscaling = false
    network_policy_config      = false
    istio_config = {
      enabled = false
      tls     = true
    }
  }

  private_cluster_config = {
    enable_private_nodes    = true
    enable_private_endpoint = false
    master_ipv4_cidr_block  = var.ip_range_controlplane
    master_global_access    = true
  }

  maintenance_config = {
    recurring_window = {
      start_time = var.maintenance_start_time
      end_time   = var.maintenance_end_time
      recurrence = var.maintenance_recurrence
    }
    daily_maintenance_window = null
    maintenance_exclusion = [
      {
        exclusion_name = "happy holidays"
        start_time     = "2021-12-24T00:00:00Z"
        end_time       = "2021-12-27T00:00:00Z"
      },
      {
        exclusion_name = "and a happy new year"
        start_time     = "2021-12-31T00:00:00Z"
        end_time       = "2022-01-02T00:00:00Z"
      }
    ]
  }

  monitoring_config = ["SYSTEM_COMPONENTS"]
  logging_config    = ["SYSTEM_COMPONENTS", "WORKLOADS"]

  dns_config = var.use_cloud_dns ? {
    cluster_dns        = "CLOUD_DNS"
    cluster_dns_scope  = "VPC_SCOPE"
    cluster_dns_domain = ""
  } : null

  authenticator_security_group = var.authenticator_security_group
  labels                       = var.cluster_resource_labels
}

module "gke-nodepool" {
  source = "github.com/terraform-google-modules/cloud-foundation-fabric//modules/gke-nodepool?ref=v8.0.0"

  project_id = var.project_id

  cluster_name = module.gke.name
  name         = var.node_pool_name
  location     = var.region

  node_disk_size    = var.disk_size_gb
  node_disk_type    = var.disk_type
  node_image_type   = "COS"
  node_machine_type = var.machine_type

  management_config = {
    auto_repair  = true
    auto_upgrade = true
  }

  autoscaling_config = {
    min_node_count = var.min_nodes
    max_node_count = var.max_nodes
  }

  node_boot_disk_kms_key = var.use_cmek ? module.kms[0].key_self_links.gke-nodes : null
  node_shielded_instance_config = {
    enable_secure_boot          = true
    enable_integrity_monitoring = true
  }

  # Use Workload Identity
  node_service_account_create = false
  node_service_account        = module.gke-service-account.email
  node_service_account_scopes = ["https://www.googleapis.com/auth/cloud-platform"]

  workload_metadata_config = "GKE_METADATA"
  node_tags                = var.node_pools_tags
}
