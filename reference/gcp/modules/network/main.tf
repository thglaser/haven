#   Copyright 2021 Google LLC
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

locals {
  subnet         = format("%s-%s-01", var.network, var.region)
  pod_range      = format("%s-%s-pods-01", var.network, var.region)
  services_range = format("%s-%s-svcs-01", var.network, var.region)
  ilb_range      = format("%s-%s-ilb-01", var.network, var.region)

  subnet_key = format("%s/%s", var.region, local.subnet)
}

# Create new VPC
module "vpc" {
  source = "github.com/terraform-google-modules/cloud-foundation-fabric//modules/net-vpc?ref=v8.0.0"

  project_id = var.project_id
  name       = var.network

  mtu = 1500

  subnets = [
    {
      ip_cidr_range = var.cidr_range
      name          = local.subnet
      region        = var.region
      secondary_ip_range = {
        (local.pod_range)      = var.cidr_range_pods
        (local.services_range) = var.cidr_range_services
      }
    },
  ]

  subnets_l7ilb = [
    {
      active        = true
      ip_cidr_range = var.cidr_range_ilb
      name          = local.ilb_range
      region        = var.region
    }
  ]

  subnet_private_access = {
    format("%s/%s", var.region, local.pod_range)      = true
    format("%s/%s", var.region, local.services_range) = true
  }
}

# Some firewall rules required for Sonobuoy's mutating webhooks and other things
module "vpc-firewall" {
  source = "github.com/terraform-google-modules/cloud-foundation-fabric//modules/net-vpc-firewall?ref=v8.0.0"

  project_id = var.project_id
  network    = module.vpc.name

  admin_ranges        = []
  ssh_source_ranges   = []
  http_source_ranges  = []
  https_source_ranges = []

  custom_rules = {
    deny-all-ingress = {
      description          = "Low-priority catch all deny ingress rule for logging."
      direction            = "INGRESS"
      action               = "deny"
      sources              = []
      ranges               = ["0.0.0.0/0"]
      targets              = []
      use_service_accounts = false
      rules                = [{ protocol = "all", ports = null }]
      logging              = "INCLUDE_ALL_METADATA"
      extra_attributes = {
        priority = 65000
      }
    }
  }
}

# Add Cloud NAT
module "nat" {
  source = "github.com/terraform-google-modules/cloud-foundation-fabric//modules/net-cloudnat?ref=v8.0.0"

  project_id     = var.project_id
  region         = var.region
  name           = format("%s-nat", module.vpc.name)
  router_network = module.vpc.name
}
