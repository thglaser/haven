#   Copyright 2021 Google LLC
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

terraform {
  required_version = ">= 1.0.0"

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 4.0.0"
    }
    google-beta = {
      source  = "hashicorp/google-beta"
      version = ">= 4.0.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = ">= 2.3.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.5.0"
    }
    time = {
      source  = "hashicorp/time"
      version = ">= 0.7.0"
    }
    random = {
      source  = "hashicorp/random"
      version = ">= 3.1.0"
    }
    null = {
      source  = "hashicorp/null"
      version = ">= 3.1.0"
    }
  }
}

# Initialize Terraform providers
provider "google" {
  impersonate_service_account = var.impersonate_service_account
}

provider "google-beta" {
  impersonate_service_account = var.impersonate_service_account
}

data "google_client_config" "provider" {}

provider "kubernetes" {
  cluster_ca_certificate = module.gke.ca_certificate
  host                   = module.gke.kubernetes_endpoint
  token                  = data.google_client_config.provider.access_token

  experiments {
    manifest_resource = true
  }
}

provider "helm" {
  kubernetes {
    cluster_ca_certificate = module.gke.ca_certificate
    host                   = module.gke.kubernetes_endpoint
    token                  = data.google_client_config.provider.access_token
  }
}

provider "time" {
}

locals {
  enable_binary_authorization = var.enable_binary_authorization
}

# Activate necessary APIs and retrieve project details
module "project" {
  source = "./modules/project"

  project_id = var.project_id
}

# Create the VPC network
module "network" {
  source = "./modules/network"

  project_id = module.project.project_id
  region     = var.region
  network    = var.network

  cidr_range          = var.cidr_range
  cidr_range_pods     = var.cidr_range_pods
  cidr_range_services = var.cidr_range_services
  cidr_range_ilb      = var.cidr_range_ilb
}

# Add Cloud Filestore instance for shared filesystem access (ReadWriteMany)
module "filestore" {
  source = "./modules/filestore"

  name       = format("%s-nfs", var.cluster_name)
  share_name = var.nfs_share_name

  project_id = module.project.project_id
  region     = var.region
  network    = module.network.network_name

  cidr_range = var.cidr_range_filestore
}

# Create regional GKE cluster
module "gke" {
  source = "./modules/gke"

  project_id     = module.project.project_id
  project_number = module.project.project_number
  region         = var.region

  cluster_name = "haven-compliance-cluster"

  gke_network              = module.network.network_name
  gke_subnet               = module.network.gke_subnet.name
  ip_range_pods            = module.network.gke_pod_subnet
  ip_range_services        = module.network.gke_services_subnet
  ip_range_controlplane    = var.cidr_range_controlplane
  enable_l4_ilb_subsetting = true

  machine_type      = var.cluster_instance_type
  max_pods_per_node = 64
  max_nodes         = 4
  min_nodes         = 1
  node_pool_name    = "gke-nodepool"

  disk_size_gb = 100
  disk_type    = "pd-balanced"

  maintenance_recurrence = "FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR"
  maintenance_start_time = "2021-10-01T02:00:00Z"
  maintenance_end_time   = "2037-10-01T02:00:00Z"

  node_pools_tags = concat([
    module.project.project_id
  ], var.compliance_testing_use ? ["sonobuoy"] : [])
  cluster_resource_labels = {
    project = module.project.project_id
  }
  authenticator_security_group = null
  enable_binary_authorization  = local.enable_binary_authorization

  # Set to enable Customer Managed Encryption Keys
  # use_cmek = true
}

# Install NFS subdirectory provider into GKE
module "gke-nfs" {
  source = "./modules/gke-nfs"

  cluster_name = module.gke.cluster_name

  nfs_host       = module.filestore.filestore_ip
  nfs_share_path = format("/%s", module.filestore.filestore_share)

  depends_on = [
    module.gke
  ]
}

# Deploy binary authorization attestors and policies
module "binary-authorization" {
  count = local.enable_binary_authorization ? 1 : 0

  source = "./modules/binauthz"

  project_id             = module.project.project_id
  compliance_testing_use = var.compliance_testing_use
}

# Add Anthos Policy Controller
module "anthos-policy-controller" {
  count = var.deploy_anthos_policy_controller ? 1 : 0

  source = "./modules/policy-controller"

  project_id   = module.project.project_id
  cluster_name = module.gke.cluster_name
  cluster_id   = module.gke.cluster_id

  admin_email            = var.admin_email
  acm_service_account    = format("service-%d@gcp-sa-anthosconfigmanagement.iam.gserviceaccount.com", module.project.project_number)
  gkehub_service_account = format("service-%d@gcp-sa-gkehub.iam.gserviceaccount.com", module.project.project_number)

  install_cis_policies = var.deploy_cis_policies

  # In case of errors from enabling Anthos features, please uncomment below:
  # enable_anthos_features = false

  depends_on = [
    module.gke
  ]
}

# Example deployment of Nginx that uses a shared NFS volume to serve docroot
module "example-deployment" {
  count = var.deploy_example_workload ? 1 : 0

  source = "./modules/example-deployment"

  depends_on = [
    module.gke-nfs,
    module.anthos-policy-controller,
  ]
}

# Add necessary resources for Haven compliance testing
module "compliance-testing" {
  source = "./modules/compliance-testing"

  project_id   = module.project.project_id
  network_name = module.gke.cluster_network

  compliance_testing_use = var.compliance_testing_use
  admin_email            = var.admin_email

  cidr_range_controlplane = var.cidr_range_controlplane

  kubernetes_endpoint = module.gke.kubernetes_endpoint
  ca_certificate      = module.gke.ca_certificate
  access_token        = data.google_client_config.provider.access_token

  depends_on = [
    module.gke,
    module.binary-authorization
  ]
}
