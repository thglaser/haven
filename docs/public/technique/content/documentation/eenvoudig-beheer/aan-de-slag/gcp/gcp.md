---
title: "Google Cloud Platform Kubernetes Engine"
path: "/aan-de-slag/gcp"
---

## Referentie Implementatie: Haven op Google Kubernetes Engine

[Google Kubernetes Engine](https://cloud.google.com/kubernetes-engine) (GKE) biedt een beheerde oplossing aan 
voor Kubernetes clusters, zowel op Google Cloud Platform (GCP) als in datacenters die beheerd worden door onze 
gebruikers. Dit laatste is mogelijk in de vorm van [Anthos](https://cloud.google.com/anthos).  GKE als product 
werd in 2015 beschikbaar gesteld aan iedereen, met Kubernetes versie 1.0. GKE is beschikbaar in alle regio’s waar 
GCP beschikbaar is, waaronder [Nederland](https://www.google.com/intl/nl/about/datacenters/locations/eemshaven/) 
(europe-west4). 

Nieuwe functies werden en worden op regelmatige basis toegevoegd. Automatisch opschalen van containers, automatisch 
herstellen van interrupties, serverless workloads via [Cloud Run](https://cloud.google.com/anthos/run/docs/setup), 
[AutoPilot](https://cloud.google.com/kubernetes-engine/docs/concepts/autopilot-overview) om het beheer van GKE clusters 
sterk te vereenvoudigen en ondersteunen van 
[Windows Server containers](https://cloud.google.com/kubernetes-engine/docs/concepts/windows-server-gke) 
zijn hier een aantal voorbeelden van. Het volledige gamma aan Anthos producten geeft klanten toegang tot een 
managed [Service Mesh](https://cloud.google.com/anthos/service-mesh), automatisch beheer van configuratie en extra controle 
op de manier waarop applicaties worden uitgerold, binnen een veiligheidskader 
([Policy Controller](https://cloud.google.com/anthos-config-management/docs/concepts/policy-controller)).

Voor meer informatie over hoe een applicatie te ontwerpen en deployen op een Kubernetes cluster op GCP, gelieve volgende 
pagina’s te bezoeken:

- [Best practices for GKE](https://cloud.google.com/kubernetes-engine/docs/best-practices)
- [Understanding IP address management in GKE](https://cloud.google.com/blog/products/containers-kubernetes/ip-address-management-in-gke)
- [Hardening your cluster's security](https://cloud.google.com/kubernetes-engine/docs/how-to/hardening-your-cluster)

![Haven op Google Kubernetes Engine](/technique/content/documentation/eenvoudig-beheer/aan-de-slag/gcp/gcp.png)

### Terraform voorbeeld

Een [code voorbeeld in Terraform](https://gitlab.com/commonground/haven/haven/-/tree/master/reference/gcp) om een
Haven compliant cluster te creëeren is nu beschikbaar, gebaseerd op best practices. Om de benodigde resources aan 
te maken is een Google Cloud Platform project nodig. Instructies om een project aan te maken en Terraform te configureren 
kan je hier terugvinden: [Project aanmaken en beheren](https://cloud.google.com/resource-manager/docs/creating-managing-projects) en 
[Terraform gebruiken voor Google Cloud resources](https://cloud.google.com/community/tutorials/getting-started-on-gcp-with-terraform).

Voor meer informatie over Google Cloud services en GKE voor Nederlandse klanten, kan je contact opnemen met ons 
op [nl-gov-cloud@google.com](mailto:nl-gov-cloud@google.com).  
