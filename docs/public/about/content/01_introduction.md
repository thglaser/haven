# Over Haven

Haven is ontstaan vanuit de behoefte om beter samen te werken. Applicaties die ontwikkeld worden, moeten makkelijk herbruikbaar zijn voor alle gemeenten. In de praktijk blijkt dit vaak lastig. Gemeenten hebben hun technische infrastructuur op verschillende manieren ingericht. Applicaties moeten daarom vaak aangepast worden aan het specifieke platform waarop ze moeten draaien.
