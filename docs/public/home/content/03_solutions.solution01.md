---
icon: flexible
---

### Wendbaar en flexibel

Met Haven maakt het niet uit welke cloud- of on-premise oplossing er wordt gebruikt. Ook kan er technisch gezien eenvoudig van leverancier worden veranderd. Migraties zijn veel simpeler.

Haven heft de onderliggende verschillen in technische infrastructuur op. Anders gezegd: Haven staat voor platform-onafhankelijke cloud infrastructuur.
