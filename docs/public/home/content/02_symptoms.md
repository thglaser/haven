---
symptoms:
  - icon: table
    text: Technische infrastructuur is op verschillende manieren ingericht
  - icon: closeSyntax
    text: Applicaties moeten vaak worden aangepast aan de specifieke infrastructuur
  - icon: mouseDrag
    text: Overstappen op een andere IT oplossing is complex
  - icon: searchPerson
    text: Grote diversiteit aan IT systemen vraagt om specialistisch beheer
---

## Applicaties die ontwikkeld worden, moeten makkelijk herbruikbaar zijn voor alle gemeenten. Waarom blijkt dit in de praktijk vaak lastig?
