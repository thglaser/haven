If the design changes, follow these steps to update the svg:

- To make text clickable, simply wrap the `<text>` tag with a `<a>` (`xmlns-xlink` in `<svg>` required)
- Use https://jakearchibald.github.io/svgomg/ to optimise  
   Check "Prettify markup", uncheck "Clean ID's"
- If it doesn't autoformat, replace all attributes with a `-` (eg `font-family`) by a camelCase notation  
   See https://stackoverflow.com/a/59793139/1783174 for easy procedure in vscode  
   Regex: /-([a-z])[a-z]+=/
- `xlink:href` can just become `href`. Other `:`-attributes can be camelCased: `xmlnsXlink`
- Id's are global, so prefix them. Replace `id="` with eg. `id="overview-`
- Update id references in links/xlinks: `#filter` becomes `#overview-filter`
- Replace any icons (eg. feature check icons) by react components
- Replace text by props
- Add `<title>` as first child of `<svg>` for accessibility  
   https://css-tricks.com/accessible-svgs/#2-inline-svg

And you're set!
