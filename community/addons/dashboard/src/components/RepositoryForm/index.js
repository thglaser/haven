// Copyright © VNG Realisatie 2019-2022
// Licensed under the EUPL
//
import React from 'react'
import { bool, object, func, string } from 'prop-types'
import { Formik } from 'formik'
import * as Yup from 'yup'
import { useTranslation } from 'react-i18next'
import { Button, Fieldset, TextInput } from '@commonground/design-system'

import { Form } from './index.styles'

const DEFAULT_INITIAL_VALUES = {
  apiVersion: 'source.toolkit.fluxcd.io/v1beta1',
  kind: 'HelmRepository',
  metadata: {
    name: 'commonground',
    namespace: 'default',
  },
  spec: {
    interval: '10m',
    url: 'https://charts.commonground.nl/',
  },
}

const validationSchema = Yup.object().shape({
  metadata: Yup.object().shape({
    name: Yup.string().required(),
    namespace: Yup.string().required(),
  }),
  spec: Yup.object().shape({
    interval: Yup.string().required(),
    url: Yup.string().required(),
  }),
})

const RepositoryForm = ({
  initialValues,
  disableForm,
  onSubmitHandler,
  submitButtonText,
}) => {
  const { t } = useTranslation()

  return (
    <Formik
      initialValues={{ ...DEFAULT_INITIAL_VALUES, ...initialValues }}
      validationSchema={validationSchema}
      onSubmit={onSubmitHandler}
    >
      {({ handleSubmit }) => (
        <Form onSubmit={handleSubmit}>
          <Fieldset>
            <TextInput name="metadata.name" size="l" disabled={!!initialValues}>
              {t('Name')}
            </TextInput>
            <TextInput
              name="metadata.namespace"
              size="l"
              disabled={!!initialValues}
            >
              {t('Namespace')}
            </TextInput>
          </Fieldset>

          <Fieldset>
            <TextInput name="spec.url" size="l">
              {t('URL')}
            </TextInput>
          </Fieldset>

          <Button type="submit" disabled={disableForm}>
            {submitButtonText}
          </Button>
        </Form>
      )}
    </Formik>
  )
}

RepositoryForm.propTypes = {
  initialValues: object,
  disableForm: bool,
  onSubmitHandler: func,
  submitButtonText: string,
}

export default RepositoryForm
