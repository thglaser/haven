// Copyright © VNG Realisatie 2019-2022
// Licensed under the EUPL
//

import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Alert } from '@commonground/design-system'
import RepositoryForm from '../../components/RepositoryForm'
import PageTemplate from '../../components/PageTemplate'
import services from '../../services'

const AddRepositoryPage = () => {
  const { t } = useTranslation()
  const [isProcessing, setIsProcessing] = useState(false)
  const [isAdded, setIsAdded] = useState(false)
  const [error, setError] = useState(null)

  const submitRepository = async (repository) => {
    setError(false)
    setIsProcessing(true)

    try {
      await services.helmRepositories.create(
        repository.metadata.namespace,
        repository,
      )
      setIsAdded(true)
    } catch (e) {
      setError(true)
      console.error(e)
    }

    setIsProcessing(false)
  }

  return (
    <PageTemplate>
      <PageTemplate.HeaderWithBackNavigation
        backButtonTo="/repositories"
        title={t('Add repository')}
      />

      {error ? (
        <Alert title={t('Failed adding repository')} variant="error">
          {error}
        </Alert>
      ) : null}

      {isAdded && !error ? (
        <Alert variant="success">{t('The repository has been added.')}</Alert>
      ) : null}

      {!isAdded ? (
        <RepositoryForm
          submitButtonText="Add"
          onSubmitHandler={submitRepository}
          disableForm={isProcessing}
        />
      ) : null}
    </PageTemplate>
  )
}

export default AddRepositoryPage
