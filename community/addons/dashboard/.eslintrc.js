// Copyright © VNG Realisatie 2019-2022
// Licensed under the EUPL
//

module.exports = {
  extends: '@commonground/eslint-config-cra-standard-prettier',
  rules: {
    'header/header': 'off',
    'jest/prefer-to-be-null': 'off',
    'jest/prefer-to-be-undefined': 'off',
    'jest/valid-describe': 'off',
  },
}
